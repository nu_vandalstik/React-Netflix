import React from 'react'

import './App.css';


import Movie from './components/Movie'
import api from './api'

function App() {
  return (
    <div className="App">
      <h1> Hello, Wisnu</h1>

      <Movie title="Netflix ORIGINALS" url={api.fetchNetflixOriginal}/>
      <Movie title="Trending Now" url={api.fetchTrending} />
      <Movie title="Top Rated" url={api.fetchTopRated} />
      <Movie title="Action Movies" url={api.fetchActionMovies} />
      <Movie title="Comedy Movies" url={api.fetchComedyMovies} />
      <Movie title="Horor Movies" url={api.fetchHorrorMovies} />
      <Movie title="Romance Movies" url={api.fetchRomanceMovies} />
      <Movie title="Documentaries Movies" url={api.fetchDocumentaries} />

    </div>
  );
}



export default App;
